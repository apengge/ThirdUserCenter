/**
 * Created by macos on 14-10-13.
 */

var request = require('supertest');

describe('AuthController', function() {

  describe('#/login', function() {

    it('check login success', function (done) {
      request(sails.hooks.http.app)
        .post('/login')
        .set('Accept', 'application/json')
        .send({ username: 'test', password: 'test' })
        .expect(200)
        .expect({ status: 200, result: 'login success!' })
        .end(function(err, res){
          if (err) return done(err);
          done();
        });
    });

    it('check login failure', function (done) {
      request(sails.hooks.http.app)
        .post('/login')
        .set('Accept', 'application/json')
        .send({ username: 'test', password: 'test1' })
        .expect(400)
        .end(function(err, res){
          if (err) return done(err);
          done();
        });
    });

    it('check request error', function (done) {
      request(sails.hooks.http.app)
        .post('/login')
        .set('Accept', 'application/json')
        .send({ name: 'test', password: 'test' })
        .expect(400, done);
    });

  });

});
