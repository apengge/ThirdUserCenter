/**
 * 用户模型测试
 * Created by jinlei on 14-10-11.
 */

var assert = require("assert")

describe('User', function(){

  describe('#find', function() {
    it('should check find function', function (done) {
      User.find()
        .then(function(results) {
          // some tests
          if (results.length > 0) {
            done();
          } else {
            done('record is not found!')
          }
        })
        .catch(done);
    });
  });

});