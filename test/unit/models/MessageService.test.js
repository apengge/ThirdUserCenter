/**
 * 用户模型测试
 * Created by jinlei on 14-10-11.
 */

var assert = require("assert");

//模拟数据
var mock = {
  "name": "email",
  "title": "邮件提醒服务",
  "description": "按你关注的规则提醒邮件到达通知",
  "icons": {
    "smallImageUrl": "111",
    "largeImageUrl": "111"
  },
  "interfaces":[
    {
      "name":"pop",
      "title":"收件服务器",
      "description": "收件服务器",
      "url":"http://xxxx",
      "actionType":"subscribeService",
      "schemaUrl":"http://xxx"
    }
  ]
};

describe('MessageService', function(){

  describe('#create', function() {


    it('- json is correct', function (done) {

      var json = _.clone(mock, true);
      MessageService.create(json)
        .exec(function(err, result){
          if (err) {

            var originalError = err.originalError;

            if (_.has(originalError, 'code') && originalError.code === 11000) {
              return done();
            } else {
              return done(require('util').inspect(err.originalError));
            }
          } else {
            return done();
          }
        });
    });


    it('- icons in json is error without largeImageUrl', function (done) {

      var json = _.clone(mock, true);
      delete json.icons.largeImageUrl;

      MessageService.create(json)
        .exec(function(err, result){
          if (err) {
            var originalError = err.originalError;
            var params = originalError.params

            for (var k in params) {
              if (params[k] === 'largeImageUrl') {
                return done();
              }
            }

            return done(originalError.message);

          } else {
            return done('It is error because it created a record');
          }
        });
    });


    it('- icons is error with a additional key', function (done) {

      var json = _.clone(mock, true);
      json.icons['x'] = 1;

      MessageService.create(json)
        .exec(function(err, result){
          if (err) {

            var originalError = err.originalError;
            if (originalError.code === 1000) {
              return done();
            } else {
              return done(err.message);
            }

          } else {
            return done('It is error because it created a record');
          }
        });
    });

    it('- interfaces is error without actionType', function (done) {

      var json = _.clone(mock, true);
      delete json.interfaces[0].actionType;

      MessageService.create(json)
        .exec(function(err, result){
          if (err) {

            var originalError = err.originalError;

            if (_.has(originalError, 'code') && originalError.code === 302) {
              return done();
            } else {
              return done(require('util').inspect(err.originalError));
            }
          } else {
            return done();
          }
        });
    });

    it('- interfaces is error with a additional key', function (done) {

      var json = _.clone(mock, true);
      json.interfaces[0].hello = 'xx';

      MessageService.create(json)
        .exec(function(err, result){
          if (err) {

            var originalError = err.originalError;

            if (_.has(originalError, 'code') && originalError.code === 1000) {
              return done();
            } else {
              return done(require('util').inspect(err.originalError));
            }
          } else {
            return done();
          }
        });
    });

  });

});