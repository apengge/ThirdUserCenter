var async = require('async'),
  log = require('captains-log')();

var TcpClient = require('../../tcpserver/tcpclient'),
  config = require('../../config/tcpserver');

var uuid = "F2A23497ABDB4D26AAB19E1EE26FF93E";

var option = {
  port: config.port,
  host: '121.42.31.180'
};

var tcpclient = new TcpClient();

tcpclient.on('error', function(err){
  log.error(err);
});

tcpclient.on('end', function(){
  process.exit(1);
});

tcpclient.connect(option, function() {
  log.info('client is connected!');

  async.series([
    //第一步， 登录
    function(next) {
      tcpclient.login({
        uuid: uuid
      }, function(err, status) {
        if (err) {
          return next(err);
        }
        if (status) {
          log.debug('login:', status)
          next(null, status);
        } else {
          return next('登录失败');
        }

      })
    },

    //第二步，发送心跳
    function(next) {

      tcpclient.heart(function(result) {
        log.debug('heart:', result);
      });

      setInterval(function() {
        tcpclient.heart(function(result) {
          log.debug('heart:', result);
        });
      }, 30000);

      next();
    },

    //第三步，发送数据
    function(next) {

      var str = 'This is a test';
      var data = new Buffer(str);

      setTimeout(function() {
        tcpclient.sendData(data.toString('hex'), function(err, status) {

          if (err) {
            log.error(err);
            return;
          }

          log.debug('send status:', status);
        });
      }, 2000);

    }

  ], function(err, results) {
    if (err) {
      throw err;
    }
  });

  //接受数据
  tcpclient.onReceived(function(msg) {
    log.info('receive: ', msg.toString());

    //回复发送状态
    return true;
  })

})