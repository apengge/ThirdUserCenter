/**
 * Created by macos on 14/12/6.
 */

function number2lowHex(number) {
  var hex = number.toString(16);

  if (hex.length < 8) {
    var fillLen = 8 - hex.length;
    for (var i = 0; i < fillLen; i++) {
      hex = "0" + hex;
    }
  }


  return hex;
}

console.log(number2lowHex(22))
