###3rdUserCenter安装
####1、依赖环境
#####nodejs 、mongodb、redis

####2、创建用户(如果已经存在则不需要重复安装)
#####创建用户
groupadd node
useradd  -g node -s /bin/tcsh -d /home/node node
修改密码：passwd node

#####设置环境变量
修改$HOME下的.cshrc文件，按照如下格式添加Nodejs环境到PATH路径中
```
setenv PATH "${PATH}:/usr/node×××××版本号×××/bin"
```
保存后，执行source .cshrc使配置生效。

####3、上传压缩包：3rdUserCenter.zip 到$HOME
####4、解压压缩包：
```
unzip 3rdUserCenter.zip
cd 3rd_usercenter

####5、安装依赖包：npm install
####6、配置：
config/local.js:
```
{
    port:20070 //配置Call服务的端口
}
```
config/connection.js:
```
localMongo: {
    adapter: 'sails-mongo',
        host: '10.163.247.97',//mongodb的ip
        port: 27017, //mongodb的端口
        user: '', //用户名
        password: '', //密码
        database: '3rdusercenter' //数据库名
}
```
config/zookeeper.js:（按照部署表格修改）
```
module.exports = {
    "zkclientIP":'10.163.247.97:2180,10.163.247.97:2181,10.163.247.97:2182',
    "applicationPath":"/application/Plat_3rd_System_001",
    "applicationTimeout":5000,
        "reportData":{
        "name":"LinkUs Platform Third Party System 001",
        "ip":"121.42.51.172",
        "port":20070
    }
}
```
config/redis.js:
```
/**
 * Created by macos on 14/11/25.
 */

module.exports = {
  host: '10.163.247.97', //redis服务ip地址
  port: 6379,
//    password:'',
  expire: 40, //session会话过期时间，单位是秒
  options: {
    detect_buffers: true
  }
}
```
config/auth.js:
```
module.exports = {
    appId:'输入appId',
    appSecret: '输入appSecret'
};
```

####7、启动：./startServer.sh
#####a.将startServer.sh和stopServer.sh转码
```
//若是提示dos2unix: Command not found.请切换到root使用  yum install dos2unix 安装 
dos2unix startServer.sh stopServer.sh
```
#####b.给startServer.sh和stopServer.sh文件赋予可执行权限
```
chmod g+x startServer.sh
chmod g+x stopServer.sh
```
#####c.执行startServer.sh脚本
```
./startServer.sh
```
注：出现forever: command not found，切换至root下执行npm install -g forever

####8、开机启动
#####a.编辑/etc/rc.local,在最后追加
```
su - node -c "cd /home/node/3rd_usercenter && ./startServer.sh &"
```