/**
 * Created by macos on 14/10/27.
 */

module.exports = {};

function reexport(name) {
	var obj = require(name);
	Object.keys(obj).forEach(function(k) {
		module.exports[k] = obj[k];
	});
}

reexport('./server');