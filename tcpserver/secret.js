/**
 * 生成加密字串
 * Created by jinlei on 14-8-16.
 */

(function(){

  var secret = {};
  var root = this;

  /**
   * 生成2位长度的16机制的公钥
   * @returns {string} 转换成字符串形式的公钥。
   */
  secret.pubKey = function pubKey() {

    var num = _.random(0, 65535);
    var hex = num.toString(16);

    var fillLen = 4 - hex.length;
    for(var i = 0;i < fillLen;i++) {
      hex = "0" + hex;
    }

    return hex;
  }

  /**
   * 生成2位长度的16进制的密钥
   * @param buffer Buffer类型的生成密钥的根数据。目前是UUID。
   * @param pubKey Buffer类型的公钥
   * @returns {string} 转换成字符串形式的密钥。
   */
  secret.secretKey = function secretKey(buffer, pubKey) {

    var sum = 0;
    for(var i=0;i< buffer.length; i++) {

      var pubKeyIndex = i % pubKey.length;
      sum += buffer[i]^pubKey[pubKeyIndex]
    }

    var a = sum & 0xff00;
    var b = sum & 0x00ff;
    var c = (b<<8)|(a>>8);

    var hex = c.toString(16);

    var fillLen = 4 - hex.length;
    for(var i = 0;i < fillLen;i++) {
      hex = "0" + hex;
    }

    return hex;
  }

  module.exports = secret;
})();