/**
 * TCP Server服务器配置
 * Created by macos on 14/10/24.
 */

module.exports = {

  port : 1733,
  clientTimeout: 60000, //TCP客户端连接超时时长，单位毫秒
  clientExpire: 40 //TCP客户端会话退休时长，单位秒
}