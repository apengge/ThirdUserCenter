/**
 * 使用189能力开放平台配置
 */

module.exports = {
	appId: '426462860000038945',
	appSecret: '2b80bea9a582f4190e7333f5fb5af2c1',
	accessTokenUrl: 'https://oauth.api.189.cn/emp/oauth2/v3/access_token?grant_type=client_credentials&app_id=%(appId)s&app_secret=%(appSecret)s',
	tokenUrl: 'http://api.189.cn/v2/dm/randcode/token?app_id=%(appId)s&access_token=%(accessToken)s&timestamp=%(timestamp)s&sign=%(sign)s',
	randcodeUrl: 'http://api.189.cn/v2/dm/randcode/sendSms?app_id=%(appId)s&access_token=%(accessToken)s&token=%(token)s&phone=%(phone)s&randcode=%(randcode)s&timestamp=%(timestamp)s&sign=%(sign)s'
}