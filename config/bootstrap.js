/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {


/*

  */
/**
   * 自动刷新快过期的session
   * @param  {Function} done [description]
   * @return {[type]}        [description]
   *//*

  function bootstrapAutosynUserService(done) {

    var service = require('../OTTSessionRefreshService');
    service.run();
    return done();
  };



  async.auto({
    autosynUser: bootstrapAutosynUserService
    createApplicationMonitor: ZkService.createApplicationMonitor
  }, cb);
*/

    async.auto({
        createApplicationMonitor: ZkService.createApplicationMonitor
    }, cb);

};
