/**
 * 用户模型
 * Created by jinlei on 14-10-11.
 */


module.exports = {
    types:{
        description:function(value) {
            return true;
        }
    },
  attributes: {
    username: {
      type: 'string',
      required: true,
      description: '用户名'
    },
    appCode: {
      type: 'string',
      required: true,
      description: 'appCode'
    },
    deadline: {
          type: 'date',
          description: '到期时间'
      },
    accessCode: {
          type: 'string',
          description: '访问码'
      },
      ttl: {
          type: 'integer',
          description: '过期时间'
      }
    }


}