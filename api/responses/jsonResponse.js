/**
 * 回复的全局json协议配置
 * Created by macos on 14-10-15.
 */

module.exports = function(status, data) {

  // Get access to `req`, `res`, & `sails`
  var req = this.req;
  var res = this.res;

  res.status(status);

  if (status === 200) {
    return res.json({status:status, result:data});
  } else {
    return res.json({status:status, error:data});
  }
};