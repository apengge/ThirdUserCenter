/**
 * 管理员用户认证
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {


  /**
   * 判断是否是超级管理员
   */
  if (req.session.authenticated
      && req.session.user
      && req.session.user.isAdmin) {
    return next();
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  return res.jsonResponse(403, 'Auth failure!');
};
