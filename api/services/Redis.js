/**
 * Created by macos on 14/11/25.
 */

var redis = require('redis');

(function(){
  var root = this;
  var redisAdapter = {};
  var _client = null;
  var _config = null;

  function _createClient(config) {

    config.port = config.port || 6379;
    config.host = config.host || 'localhost';
    config.db = config.db || 0;
    config.debug_mode = config.debug_mode || true;

    _client = redis.createClient(config.port, config.host, config.options);
    _client.select(config.db);

    if (config.password) {
      root._client.auth(config.password);
    }

    _client.on("error", function (err) {
      sails.log.error(err);
    });
  }

  redisAdapter.createClient = function (config) {
    _config = _config || config;
    _createClient(_config);
  }

  redisAdapter.client = function() {

    if (_client == null || _client.connected == false) {
      _createClient(_config);
    }

    return _client;
  }

  module.exports = redisAdapter;
})();