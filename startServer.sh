#!/bin/bash
CURRENT_PATH=$(cd "$(dirname "$0")"; pwd)

rm -f *.log
forever -p . -l ./forever.log -o ./out.log -e ./error.log start $CURRENT_PATH/app.js
