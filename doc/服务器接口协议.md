接口协议
==============

## Postman协议范例地址

https://www.getpostman.com/collections/c18081b2e45ed84c1a3b

所有接口采用Restful风格的socket.io和http接口

## 响应返回的协议说明

* 所有的响应返回参考HTTP协议要求编写。协议格式要求如下描述。
* 服务器返回时，status含义与HTTP协议一样。返回status同时，也必须按HTTP协议把状态写入到http header中。
  * 200，正确返回。
  * 400，客户端请求错误。
  * 403 请求被禁止
  * 404 服务不存在
  * 500 服务器内部错误
  * 其他请参考HTTP协议的状态说明。
* result，表示正确返回的内容。具体内容根据不同服务规定，必须是JSON或者字符串。
* error, 表示错误返回。具体内容根据不同服务规定，必须是JSON或者字符串。
* 对于用户认证，其正确返回不是result，而是其他应用的返回。

### 响应返回的报文内容格式:

正确响应内容
```
{
  "status" : 200,
  "result" : ""
}
```

失败响应内容
```
{
  "status" : 400,
  "error"  : ""
}
```

## 1 用户

### 1.1 用户登陆

#### 接口地址:
http://121.42.31.180:17001/login

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
  "username" : "",
  "password" : ""
}
```

### 1.2 用户退出

#### 接口地址:
http://121.42.31.180:17001/logout

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{}
```

### 1.3 用户认证

当访问任意一个需要认证的接口，首先调用该接口。该接口会根据用户认证结果，自动返回

#### 接口地址:
服务器任意接口

#### 通讯方式:

HTTP  或者 socket.io

#### 接口协议:

提交内容
```
其他接口的接口协议
```

正确响应内容
```
其他接口的协议内容
```

失败响应内容
```
{
  "status" : 403,
  "error"  : ""
}
```

### 1.4 会员注册用户名校验

http://121.42.31.180:17001/member/validateUser

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
  "username" : ""
}
```

### 1.5 会员账号信息修改

#### 接口地址:
http://121.42.31.180:17001/member/modifyMember

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
  "username" : "",
    ...
}
```

### 1.6 会员账号密码修改

#### 接口地址:
http://121.42.31.180:17001/member/modifyPass

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
 {
   "username" : "",
   "password" : "",
   "newPassword" : ""
 }
```

### 1.7 获取当前登录用户信息

#### 接口地址:
http://121.42.31.180:17001/member/getCurrentUser

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
 {

 }
```

## 2 消息

### 2.1 消息创建

此项接口只用于服务器端程序

#### 接口地址:
http://121.42.31.180:17001/message/send

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
  "toUserNames" : ["admin"],
  "fromUserName" : "admin",
  "serviceName" : "reminder",
  "content" : {
    "title" : "测试",
    "id":"5429181a79b75c221b06230e"
  }
}
```

### 2.2 客户端开启服务器消息监听

此项接口只用于客户端

#### 接口地址:
http://121.42.31.180:17001/message/send?action=watch

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{}
```

### 2.3 客户端消息监听

* 此项接口只用于客户端。
* 客户端必须先调用《2.2 客户端开启服务器消息监听》，才能开启客户端消息监听。
* 每当服务器《2.1 消息创建》被调用时，服务器会自动推送消息到客户端。

#### 接口地址:

* 服务器地址:121.42.31.180
* 服务器端口:17001
* 事件: message

#### 通讯方式:

socket.io

#### 接口协议:

响应内容（服务器自动推送客户端的消息内容）

```
{
  "name":"message",
  "args":[{
      "verb":"created",
      "data":{
        "id":"5428d2e5ee96786f6f1c8745",
        "toUserNames":["test"]
        }],
  "id":"5428d2e5ee96786f6f1c8745"
}

```

### 2.4 消息接受

* 此项接口只用于客户端。
* 当消息监听到内容后，可以用接受到的内容按下面接口协议的提交内容格式到服务器获取数据。

#### 接口地址:
http://121.42.31.180:17001/message/receive

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
 "toUserNames" : "admin",
 "id" : "542919a672ba5f6a1c03338a"
}
```

### 2.5 查找多个消息

* 此项接口只用于客户端。
* 用户可以查询到自己的多个消息。

#### 接口地址:
http://121.42.31.180:17001/message/findToMe

#### 通讯方式:

HTTP POST 或者 socket.io.post

#### 接口协议:

提交内容
```
{
  "where": {"toUserNames" : "admin"},
  "limit" : 3
}
```

## 3 消息服务

### 3.1 应用认证（暂无）

某个已注册的消息服务，在调用消息服务器（NotificationServer）的某个接口时，需要认证通过后才能调用。

### 3.1 应用注册（暂无）

## 4 文件服务

对应视频、音频、图片等大数据容量的文件，进行统一存储。

### 接口地址

* 文件服务器地址：121.42.31.180
* 文件服务器端口：22121

### 通讯方式

TCP socket

### 接口协议

Socket提交内容

```
MagicNumberParametersLengthParametersFileStream

MagicNumber：字节数组。固定为@Beeper@
ParametersLength: Parameters的字节长度，4个字节。
Parameters: 附加参数字节数组。如：  {command:'upload',fileSize:" + fileData.length + ",ext:'txt',param2:2}
FileStream: 文件字节数组。
```

服务器返回

```
{
  "retCode":"",
  "success":true,
  "retData":null,
  "msg":"http://121.42.31.180:22123/group1/M00/00/01/oYYBAFRHVTuARpwDAAACjNzHUo0540.txt"
}
```

### 实例JAVA代码


```
package com.gfound.beeper.storage.fastdfs; 

import java.io.FileInputStream; 
import java.io.IOException; 
import java.net.Socket; 
import java.nio.ByteBuffer; 

public class Client { 

public static final String MSG_SIGNED = "@Beeper@"; 

private static final String SERVER_IP = "121.42.31.180"; 
public static final Integer SERVER_PORT = 22121; 

public static void main(String[] args) throws Exception { 
Socket socket = null; 
try { 

/** 
* Socket通信约定 
* 客户端需要发送的信息： 
* ①字节数组	②[Parameters长度][4个字节]	③附加参数字节数组	④文件字节数组 
* MagicNumber ParametersLength Parameters FileStream 
*/ 
FileInputStream fileInputStream = new FileInputStream("D:\\1.txt"); 

//将文件转换为字节数组，④号位 
byte[] fileData = StreamUtils.input2byte(fileInputStream); 

//信息头 转换为字节数组，①号位 
byte[] msgSignedBytes = MSG_SIGNED.getBytes(); 

//包装附加的参数信息，为JSON格式 
String parameters = "{command:'upload',fileSize:" + fileData.length + ",ext:'txt',param2:2}"; 

//将参数信息转换为字节数组，③号位 
byte[] parametersBytes = parameters.getBytes(); 

//获取参数字节数组长度 
Integer parametersSize = parametersBytes.length; 

//此长度写入②号位 
byte[] parametersSizeBytes = intToByteLH(parametersSize); 

//根据①②③④总长度分配待发送字节数组的长度，此处根据具体的编程语言定义 
//需要保证无冗余的0 
Integer allocateSize = msgSignedBytes.length + parametersSizeBytes.length + parametersBytes.length + fileData.length; 
ByteBuffer byteBuffer = ByteBuffer.allocate(allocateSize); 

//封装待发送的字节信息 
byteBuffer.put(msgSignedBytes);	//① 
byteBuffer.put(parametersSizeBytes);	//② 
byteBuffer.put(parametersBytes);	//③ 
byteBuffer.put(fileData);	//④ 

byte[] sendByte = byteBuffer.array(); 

socket = new Socket(SERVER_IP, SERVER_PORT); 
socket.getOutputStream().write(sendByte); 
socket.getOutputStream().flush(); 

//接收服务器回应的信息 
//服务器回应的信息为字节数组，解码后为JSON字符串，结构和RetMsg一样，success代表是否成功；为true，msg为文件url，否则为出错信息 
byte[] retMsgbytes = StreamUtils.input2byte(socket.getInputStream()); 
System.out.println(new String(retMsgbytes)); 
} catch(IOException ex) { 
ex.printStackTrace(); 
} finally { 
try { 
socket.close(); 
} catch(Exception ex) {} 
} 

} 

private static byte[] intToByteLH(int n) 
{ 
byte[] b = new byte[4]; 
b[0] = (byte) (n & 0xff); 
b[1] = (byte) (n >> 8 & 0xff); 
b[2] = (byte) (n >> 16 & 0xff); 
b[3] = (byte) (n >> 24 & 0xff); 
return b; 
} 
} 

```